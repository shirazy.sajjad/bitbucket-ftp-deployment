<?php
	return array(
		'items'=>array(
			'bin/',
			'css/',
			'js/',
			'obj/',
			'SQL/',
			'README.md',
			'.gitignore'
			),
		'exceptions'=>array(
			'css/all.min.css',
			'js/all.min.js'
			)
	);
?>